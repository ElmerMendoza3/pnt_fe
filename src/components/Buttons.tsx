"use client";
import { Button } from "@mui/material";
import { useRouter } from "next/navigation";
const apiUrl = process.env.NEXT_PUBLIC_API_URL;
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
function Buttons({ newsId }: { newsId: number }) {
  const router = useRouter();

  return (
    <div style={{display: 'flex', gap: '10px'}}>
      <Button
        variant="outlined"
        color="error"
        onClick={async () => {
          if (confirm("are you sure you want to delete this prodcut?")) {
            try {
                const params = newsId ? `/${newsId}` : '';
                const response = await fetch(`${apiUrl}/news${params}`, {
                  method: 'DELETE',
                  headers: {
                    'Content-Type': 'application/json',
                  },
                });
                router.refresh();
                router.push('/dashboard/news');
                if (response.ok ){
                    router.push("/dashboard/news");
                    router.refresh();
                    }
              } catch (error) {
                console.error('Error fetching data:', error);
              }
               
          }
        }}
      >
        <DeleteIcon/>
      </Button>{" "}
      <Button
        variant="contained"
        color="primary"
        onClick={() => {
          router.push(`/dashboard/news/add/${newsId}`);
        }}
      >
        <EditIcon/>
      </Button>
    </div>
  );
}

export default Buttons;