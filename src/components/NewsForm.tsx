"use client";
import { Button, TextField } from "@mui/material";
import { useRouter } from "next/navigation";
import { useParams } from "next/navigation";

import React, { useEffect, useState } from "react";

const apiUrl = process.env.NEXT_PUBLIC_API_URL;
const initialState = {
  title: "",
  url_image: "",
  place: "",
  author: "",
  content: "",
};
function NewsForm() {
  const [formData, setFormData] = useState(initialState);
  const param = useParams();
  const router = useRouter();
  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const method = param.id ? "PUT" : "POST";
    const params = param.id ? `/${param.id}` : "";
    try {
      const response = await fetch(`${apiUrl}/news${params}`, {
        method: method,
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(formData),
      });
      router.refresh();
      router.push("/dashboard/news");
      if (response.ok) {
      } else {
        console.error("An error occurred:", response.statusText);
      }
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  useEffect(() => {
    const getNews = async () => {
      if (param.id) {
        const rest = await fetch(`${apiUrl}/news/${param.id}`);
        const data = await rest.json();
        setFormData(data);
      }
    };
    getNews();
  }, []);

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <TextField
          label="Titulo"
          name="title"
          value={formData.title}
          onChange={handleChange}
          margin="normal"
          fullWidth
          required
        />
        <TextField
          label="Autor"
          name="author"
          value={formData.author}
          onChange={handleChange}
          margin="normal"
          fullWidth
          required
        />
        <TextField
          label="Contenido"
          name="content"
          value={formData.content}
          onChange={handleChange}
          type="text"
          multiline
          minRows={4}
          fullWidth
          required
        />

        <TextField
          label="Lugar"
          name="place"
          value={formData.place}
          onChange={handleChange}
          margin="normal"
          fullWidth
          required
        />

        <TextField
          label="Url_image"
          name="url_image"
          value={formData.url_image}
          onChange={handleChange}
          margin="normal"
          fullWidth
        />
        <Button type="submit" variant="contained" color="primary">
          {param.id ? "Update News" : "Add News"}
        </Button>
      </form>
    </div>
  );
}

export default NewsForm;
