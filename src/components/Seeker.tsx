import { Button, TextField } from '@mui/material'
import React from 'react'
import styles from '@/app/page.module.css'
interface Props{
  updateNews: (value: string) => void
  toggleSort: () => void
} 
function Seeker({ updateNews, toggleSort }: Props) {
  return (
    <section className={styles.seeker}>
      <div style={{display: 'flex', gap: '10px'}}>
        <TextField
          id="outlined-basic"
          label="Search"
          placeholder='Buscar por titulo'
          variant="outlined"
          onChange={e => updateNews(e.target.value)}
          fullWidth
        />
        <Button variant="outlined" onClick={() => toggleSort()}>Ordenar por Titulo</Button>
      </div>
    </section>
  )
}

export default Seeker