import { News } from '@/models/models'
import React from 'react'
import DateRangeIcon from '@mui/icons-material/DateRange';
import PlaceIcon from '@mui/icons-material/Place';
import PersonIcon from '@mui/icons-material/Person';
import styles from '@/app/page.module.css'
import noImage from '../assets/noimage.png'
interface Props{
    news: News
}
/* const styles = {
    width: '200px',
} */
function NewsCard({news}: Props) {
  return (
    <div className={styles.card}>
      <div className={styles.cardImage}>
        {
          news.url_image.includes('https') ? <img  src={news.url_image} alt={news.title} /> :
            <img src={noImage.src} alt={news.title} />
        }
      </div>
      <div className={styles.cardContent}>
        <h3>{news.title}</h3>
        <p>{news.content}</p>
        <p className={styles.cardText}><PersonIcon/> {news.author}</p>
        <p className={styles.cardText}> <PlaceIcon/> {news.place}</p>
        <p className={styles.cardText} > <DateRangeIcon/> 
            {news.publication_date?.slice(0, 10) + ' ' + news.publication_date?.slice(11, 19)}
        </p>
      </div>
    </div>
  )
}

export default NewsCard