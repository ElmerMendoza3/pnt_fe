'use client'

import React from 'react'
import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from "@mui/material";
import Buttons from './Buttons';
import { News } from '@/models/models';

interface Props {
  news: News[]
}
export const dynamic = 'force-dynamic'
function NewsTable({news}: Props) {
  return (
    <TableContainer >
    <Table >
      <TableHead>
        <TableRow>
          <TableCell>Titulo</TableCell>
          <TableCell>Contenido</TableCell>
          <TableCell>Autor</TableCell>
          <TableCell>Lugar</TableCell>
          <TableCell>Fecha de publicación</TableCell>
          <TableCell>Url Image</TableCell>
          <TableCell>Acciones</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {news.map((news: News) => (
            <TableRow key={news.id}>
            <TableCell>{news.title}</TableCell>
            <TableCell>{news.content}</TableCell>
            <TableCell>{news.author}</TableCell>
            <TableCell>{news.place}</TableCell>
            <TableCell>{news.publication_date}</TableCell>
            <TableCell style={{minWidth: '30px', maxWidth: '30px'}}>
                <img style={{width: '100%'}}  src={news.url_image} alt=""/>
            </TableCell>
            <TableCell>
              <Buttons newsId={news.id}/>
            </TableCell>
         </TableRow>
        ))}
      </TableBody>
    </Table>
  </TableContainer>
  )
}

export default NewsTable