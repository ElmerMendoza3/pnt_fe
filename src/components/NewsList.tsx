'use client'
import { News } from "@/models/models";
import styles from '@/app/page.module.css'
import { Suspense, useEffect, useRef, useState } from "react";
import Seeker from "./Seeker";
import NewsCard from "./NewsCard";
export const dynamic = 'force-dynamic'

interface Props{
    newsList: News[]
}
interface AppState {
  news: News[]
}
function NewsList({newsList}: Props) {
  const [news, setNews] = useState<AppState['news']>([])
  const newsRef = useRef<AppState['news']>([]);
  const [isSorted, setIsSorted] = useState(false)
  const updateNews = (value: string) => {
    const newsFiltered = newsRef.current.filter((news: News) => news.title.toLowerCase().includes(value))
    setNews(newsFiltered)
  }

  const handleToggleSort = () => {
   setIsSorted(!isSorted)
  }

  useEffect (()=> {
    setNews(newsList)
    newsRef.current = newsList
  }, [])
  
  const newsSorted = isSorted ? news.toSorted((a, b) => a.title.localeCompare(b.title)) : news

  const cards = news.length > 0 ? newsSorted.map((news: News) => (
    <NewsCard news={news} key={news.id} />
  )) : <p>No news</p>

  return (
    <>
        <Seeker updateNews={updateNews} toggleSort={handleToggleSort}/>
        <div className={styles.news}>
          <Suspense fallback={<p>Loading...</p>}>
            {cards}
          </Suspense>
        </div>
    </>
  )
}

export default NewsList
