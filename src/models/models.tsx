export type News = {
    id: number,
    title: string,
    description: string
    url_image: string
    publication_date: string
    place: string
    author: string
    content: string
}