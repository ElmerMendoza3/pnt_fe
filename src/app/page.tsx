import { Suspense } from 'react';
import styles from './page.module.css'
import NewsList from '@/components/NewsList';
const apiUrl = process.env.NEXT_PUBLIC_API_URL;

const useNews = async () => {
  try {
    const rest = await fetch(`${apiUrl}/news`)
    const data = await rest.json()
    return data;
} catch (error) {
    console.error('Data fetch error', error);
    throw new Error('Error fetching data');
}
}

export const dynamic = 'force-dynamic'

async function Home() {
  const news = await useNews();

  return (
    <>
      <h3 className={styles.title}>Portal el HOCIÓN</h3>
      <main className={styles.main}>
        <Suspense fallback={<p>Loading...</p>}>
          <NewsList newsList={news}/>
        </Suspense>
      </main>
    </>
  )
}

export default Home