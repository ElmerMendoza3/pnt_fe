import type { Metadata } from 'next'
import { Inter } from 'next/font/google'
import './globals.css'

const inter = Inter({ subsets: ['latin'] })

export const metadata: Metadata = {
  title: 'Portal el HOCICÓN',
  description: 'Tu portal de noticias confiable y actualizado. Descubre las últimas noticias, reportajes y análisis en tiempo real. Mantente informado sobre eventos globales, política, tecnología, entretenimiento y más.',
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <body className={inter.className}>
        {children}
      </body>
    </html>
  )
}
