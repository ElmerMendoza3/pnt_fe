import SiderBar from "./SiderBar"
import styles from '@/app/page.module.css'
export default function Layout({ children}: {
    children: React.ReactNode
}) {
    return (
        <div>
            <div>
             <SiderBar/>
            </div>
            <div className={styles.main}>
                {children}
            </div>
        </div>
    )
}
