import Skeleton from '@mui/material/Skeleton';
function Loading(){
    return (
        <div>
            <Skeleton height={40}/>
            <Skeleton height={40}/>
            <Skeleton height={40}/>
            <Skeleton height={40}/>
        </div>        
    )
} 
export default Loading