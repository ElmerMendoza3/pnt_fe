import NewsTable from "@/components/NewsTable";
import { Button } from "@mui/material";
import Link from "next/link";
const apiUrl = process.env.NEXT_PUBLIC_API_URL;

export const dynamic = 'force-dynamic'
async function fetchNews () {
  try {
      const rest = await fetch(`${apiUrl}/news`)
      const data = await rest.json()
      return data;
  } catch (error) {
      console.error('Data fetch error', error);
      throw new Error('Error fetching data');
  }
}


export async function DashboardPage() {
  const news = await fetchNews();
  return (
    <div>
      <h3>News</h3>
      <Link href="/dashboard/news/add">
        <Button variant="contained" color="success">Add News</Button>
      </Link>
        <NewsTable news={news} />
    </div>
  );
}
export default DashboardPage;
